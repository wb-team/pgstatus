<?php

/**
 * Copyright 2010-2012 © Mehdi Dogguy <mehdi@debian.org>
 * Copyright 2015 © Joachim Breitner <nomeata@debian.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

require_once("library.php");
db_connect();

list($pkgs, $archs, $suite, $limit, $bad_results_only) =
  sanitize_params("packages", "archs", "dist", "limit", "bad_results_only");

# This looks nicer in the form, and makes the query smaller
if ($archs === $ARCHS) { $archs = array(); };

html_header(sprintf("Recent build results"));

echo "<div id=\"body\">\n";
select_recent_logs($archs, $pkgs, $suite, $limit, $bad_results_only);

printf("<h3>Recent build logs</h3>");

$resolved_pkgs = array();
foreach($pkgs as $p) {
	if (preg_match('/@/', $p)) {
		$maint_pkgs = grep_maintainers($p, "yes");
		if (empty($maint_pkgs)) {
			printf("<p>No packages found for maintainer address \"%s\".</p>", $p);
		}
		$resolved_pkgs = array_merge($resolved_pkgs, $maint_pkgs);
	} else {
		array_push($resolved_pkgs, $p);
	}
}
sort($resolved_pkgs);
$resolved_pkgs = array_unique($resolved_pkgs);

$query = recent_logs_query($resolved_pkgs, $archs, $suite, $limit, $bad_results_only);
$query_result = pg_query($dbconn, $query);

if ($query_result) {

echo '<table class="data logs"><tr>
  <th>Package</th>
  <th>Version</th>
  <th>Suite</th>
  <th>Architecture</th>
  <th>Result</th>
  <th>Build date</th>
  <th>Builder</th>
  <th>Build time</th>
  <th>Disk space</th>
  </tr>';
while($r = pg_fetch_assoc($query_result)) {
  $last = $r;
  $pkg = sprintf("<a href=\"package.php?p=%s&suite=%s\">%s</a>", urlencode($r["package"]), urlencode($r["distribution"]), $r["package"]);
  $ver = logs_link($r["package"], "", $r["version"], $r["distribution"], $r["version"]);
  $arch = logs_link($r["package"], $r["arch"], "", $r["distribution"], $r["arch"]);
  $result = color_text("Maybe-".ucwords($r["result"]), $r["result"] != "successful");
  $link = build_log_link($r["package"], $r["arch"], $r["version"], strtotime($r["timestamp"]), 0, $result);

  if (empty($r["builder"])) {
    $builder = no_empty_text("");
  } else {
    $builder = pkg_buildd(buildd_realname($r["builder"], $r["arch"]), check_suite($suite), $r["arch"]);
  }
  $duration = date_diff_details(0, $r["build_time"]);
  $disk_space = logsize($r["disk_space"]);

  printf("<tr>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          <td>%s</td>
          </tr>\n",
          $pkg,
          $ver,
          $r["distribution"],
          $arch,
          $link,
          $r["timestamp"],
          $builder,
          no_empty_text($duration[1]),
          $disk_space);
}
echo "</table>";

printf("<h3>Download build logs</h3>");
echo html_download_logs($last["package"], $last["version"]);
} else {
  print(pg_last_error());
}
echo "</div>\n";
if (count($archs) == 1) {
  echo "See as well UDD interface for <a href='https://udd.debian.org/cgi-bin/ftbfs.cgi?arch=$archs[0]'>$archs[0] FTBFS list</a>.\n";
}

html_footer();
?>
